# https://hub.docker.com/r/jenkinsci/jnlp-slave/
FROM jenkinsci/jnlp-slave:3.35-2-alpine
MAINTAINER aruba-get

ARG VCS_REF
ARG BUILD_DATE

# Metadata
LABEL org.label-schema.vcs-ref=$VCS_REF \
  org.label-schema.vcs-url="https://gitlab.com/hpe-aruba-get-public/jnlp-slave" \
  org.label-schema.build-date=$BUILD_DATE \
  org.label-schema.docker.dockerfile="/Dockerfile"

# The default user for base image is not root.  We also need to update the $HOME directory for pip cache.
USER root
ENV HOME /root

ENV HELM_VERSION v2.12.1
ENV HELM3_VERSION v3.6.3
ENV KUBECTL_VERSION v1.15.0

# Section from official nodeJs Dockerfile https://github.com/nodejs/docker-node/blob/main/12/alpine3.11/Dockerfile
# Modified the user and group id from 1000 to 1001 because Jenkins is using 1000.
# CHECKSUM from https://unofficial-builds.nodejs.org/download/release
ENV NODE_VERSION 13.8.0

RUN addgroup -g 1001 node \
    && adduser -u 1001 -G node -s /bin/sh -D node \
    && apk add --no-cache \
        libstdc++ \
    && apk add --no-cache --virtual .build-deps \
        curl \
    && ARCH= && alpineArch="$(apk --print-arch)" \
      && case "${alpineArch##*-}" in \
        x86_64) \
          ARCH='x64' \
          CHECKSUM="3325ec0bff602d30222e0c6c6af81aefa04d7d20946bac65d55dbb09d7d7e1e0" \
          ;; \
        *) ;; \
      esac \
  && if [ -n "${CHECKSUM}" ]; then \
    set -eu; \
    curl -fsSLO --compressed "https://unofficial-builds.nodejs.org/download/release/v$NODE_VERSION/node-v$NODE_VERSION-linux-$ARCH-musl.tar.xz"; \
    echo "$CHECKSUM  node-v$NODE_VERSION-linux-$ARCH-musl.tar.xz" | sha256sum -c - \
      && tar -xJf "node-v$NODE_VERSION-linux-$ARCH-musl.tar.xz" -C /usr/local --strip-components=1 --no-same-owner \
      && ln -s /usr/local/bin/node /usr/local/bin/nodejs; \
  else \
    echo "Building from source" \
    # backup build
    && apk add --no-cache --virtual .build-deps-full \
        binutils-gold \
        g++ \
        gcc \
        gnupg \
        libgcc \
        linux-headers \
        make \
        python3 \
    # gpg keys listed at https://github.com/nodejs/node#release-keys
    && for key in \
      4ED778F539E3634C779C87C6D7062848A1AB005C \
      94AE36675C464D64BAFA68DD7434390BDBE9B9C5 \
      74F12602B6F1C4E913FAA37AD3A89613643B6201 \
      71DCFD284A79C3B38668286BC97EC7A07EDE3FC1 \
      8FCCA13FEF1D0C2E91008E09770F7A9A5AE15600 \
      C4F0DFFF4E8C1A8236409D08E73BC641CC11F4C8 \
      C82FA3AE1CBEDC6BE46B9360C43CEC45C17AB93C \
      DD8F2338BAE7501E3DD5AC78C273792F7D83545D \
      A48C2BEE680E841632CD4E44F07496B3EB3C1762 \
      108F52B48DB57BB0CC439B2997B01419BD92F80A \
      B9E2F5981AA6E0CD28160D9FF13993A75599653C \
    ; do \
      gpg --batch --keyserver hkps://keys.openpgp.org --recv-keys "$key" || \
      gpg --batch --keyserver keyserver.ubuntu.com --recv-keys "$key" ; \
    done \
    && curl -fsSLO --compressed "https://nodejs.org/dist/v$NODE_VERSION/node-v$NODE_VERSION.tar.xz" \
    && curl -fsSLO --compressed "https://nodejs.org/dist/v$NODE_VERSION/SHASUMS256.txt.asc" \
    && gpg --batch --decrypt --output SHASUMS256.txt SHASUMS256.txt.asc \
    && grep " node-v$NODE_VERSION.tar.xz\$" SHASUMS256.txt | sha256sum -c - \
    && tar -xf "node-v$NODE_VERSION.tar.xz" \
    && cd "node-v$NODE_VERSION" \
    && ./configure \
    && make -j$(getconf _NPROCESSORS_ONLN) V= \
    && make install \
    && apk del .build-deps-full \
    && cd .. \
    && rm -Rf "node-v$NODE_VERSION" \
    && rm "node-v$NODE_VERSION.tar.xz" SHASUMS256.txt.asc SHASUMS256.txt; \
  fi \
  && rm -f "node-v$NODE_VERSION-linux-$ARCH-musl.tar.xz" \
  && apk del .build-deps \
  # smoke tests
  && node --version \
  && npm --version

ENV YARN_VERSION 1.22.15

RUN apk add --no-cache --virtual .build-deps-yarn curl gnupg tar \
  && for key in \
    6A010C5166006599AA17F08146C2130DFD2497F5 \
  ; do \
    gpg --batch --keyserver hkps://keys.openpgp.org --recv-keys "$key" || \
    gpg --batch --keyserver keyserver.ubuntu.com --recv-keys "$key" ; \
  done \
  && curl -fsSLO --compressed "https://yarnpkg.com/downloads/$YARN_VERSION/yarn-v$YARN_VERSION.tar.gz" \
  && curl -fsSLO --compressed "https://yarnpkg.com/downloads/$YARN_VERSION/yarn-v$YARN_VERSION.tar.gz.asc" \
  && gpg --batch --verify yarn-v$YARN_VERSION.tar.gz.asc yarn-v$YARN_VERSION.tar.gz \
  && mkdir -p /opt \
  && tar -xzf yarn-v$YARN_VERSION.tar.gz -C /opt/ \
  && ln -s /opt/yarn-v$YARN_VERSION/bin/yarn /usr/local/bin/yarn \
  && ln -s /opt/yarn-v$YARN_VERSION/bin/yarnpkg /usr/local/bin/yarnpkg \
  && rm yarn-v$YARN_VERSION.tar.gz.asc yarn-v$YARN_VERSION.tar.gz \
  && apk del .build-deps-yarn \
  # smoke test
  && yarn --version

# Following is ASP usual customization
RUN \
  # Install curl-dev on alpine 3.4 due to http://stackoverflow.com/a/41651363/684893
  apk add --no-cache docker curl curl-dev gettext openssh \
  # Use latest GNU tar since busybox tar version is missing useful features.
  && apk --update add tar \
  # Install Helm2
  && curl -fsSL -o helm.tar.gz https://get.helm.sh/helm-${HELM_VERSION}-linux-amd64.tar.gz \
  && tar -C /usr/local/ -xzf helm.tar.gz \
  && cp /usr/local/linux-amd64/helm /usr/local/bin/ \
  && rm -fr /usr/local/linux-amd64 \
  # Install Helm3
  && curl -fsSL -o helm3.tar.gz https://get.helm.sh/helm-${HELM3_VERSION}-linux-amd64.tar.gz \
  && tar -C /usr/local/ -xzf helm3.tar.gz \
  && cp /usr/local/linux-amd64/helm /usr/local/bin/helm3 \
  && rm -fr /usr/local/linux-amd64 \  
  # Install kubectl as per https://kubernetes.io/docs/user-guide/prereqs/
  # && curl -LO https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl \
  && curl -fsSLO https://storage.googleapis.com/kubernetes-release/release/${KUBECTL_VERSION}/bin/linux/amd64/kubectl \
  && chmod +x ./kubectl \
  && mv ./kubectl /usr/local/bin/kubectl \
  # Install Python and NodeJS base dependencies and build dependencies.
  && apk add --no-cache python py-pip \
  && apk add --no-cache --virtual .build-dependencies build-base git jq npm \
  # Create a python virtualenv for future caching purposes.  Don't actually create a virtualenv yet because then we can't cache the pip packages.
  # Install awscli pip package so we can do automated AWS ECR logins.
  && pip install --upgrade pip virtualenv awscli \
  # Install yarn through deprecated npm method.
  #   Alpine will have a yarn package in future, https://pkgs.alpinelinux.org/packages?name=yarn&branch=&repo=&arch=&maintainer=
  && npm install -g serverless --quiet --progress=false --color false \
  # Cleanup
  && rm -rf /var/cache/apk/*

ENV LANG=C.UTF-8 VIRTUAL_ENV=/root/venv PATH=/root/venv/bin:$PATH PYTHONPATH=/usr/lib/python2.7/site-packages/

WORKDIR /root/app

